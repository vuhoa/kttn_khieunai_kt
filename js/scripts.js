$(document).ready(function() {
	 $(window).scroll(function(){
	 	var height = $(window).scrollTop();
	 	if(height >50){
	 		$('.keep').css('top',0);
	 	}else{
	 		$('.keep').css('top',67);
	 	}
	 })
    $('.header_right_top').click(function(e){
        e.preventDefault();
        $('.header_right ul').slideToggle();
    });
    $('.keep ul li:first-child').click(function(e){
    	e.preventDefault();
    	$('.keep').toggleClass('extendMenu');
    	$('.site-wrap').toggleClass('toggled');
    	$('.sidebar-submenu').css('display','none');
    });
    $('.keep ul li:not(:first-child) > a').click(function(e){
    	e.preventDefault();
    	$('.keep').addClass('extendMenu');
    	$('.site-wrap').addClass('toggled');
    	$('.sidebar-submenu').slideUp(200);
    	if(
    		$(this).parent().hasClass("active")
    	){
		 $(".sidebar-dropdown").removeClass("active");
		    $(this)
		      .parent()
		      .removeClass("active");
		  } else {
		    $(".sidebar-dropdown").removeClass("active");
		    $(this).next(".sidebar-submenu").slideDown(200);
		    $(this).parent().addClass("active");
		  }
    })

     $('#example').dataTable({
        "lengthMenu": [
            [10, 15, 25, 50, -1],
            [10, 15, 25, 50, "All"]
        ],
        "dom": 't<"bottom-table"<"right"ip>><"clear">',

        "language": {
            "sProcessing": "Đang xử lý...",
            "sLengthMenu": "Xem _MENU_",
            "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
            "sInfo": "Trang _START_ đến _END_ (_TOTAL_ bản ghi)",
            "sInfoEmpty": "Trang 0 đến 0 (0 bản ghi)",
            "sInfoFiltered": "(được lọc từ _MAX_ bản ghi)",
            "sInfoPostFix": "",
            "sSearch": "Tìm:",
            "oPaginate": {
                "sNext": '<img src="images/next.svg" height="9px" width="5px"/>',
                "sPrevious": '<img src="images/pre.svg" height="9px" width="5px"/>',
                "sFirst": '<img src="images/pre_all.svg" height="9px" width="5px"/>',
                "sLast": '<img src="images/next-all.svg" height="9px" width="5px"/>'
            },
            "responsive": true
        }
    });
});